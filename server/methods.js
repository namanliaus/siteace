Meteor.methods({
  requestURL: function(url) {
    this.unblock();
    try {
      var result = HTTP.get(url);
      if (result.statusCode === 200) {
        return result.content;
      }
      throw new Meteor.Error(500, "Error happened while fetching data from the url. Status code: " + result.statusCode);
    } catch(e) {
      console.log(e);
      throw new Meteor.Error(500, "Error happened while fetching data from the url.");
    }
  },
  search: function(kw, limit) {
    this.unblock();
    if (!limit) {
      limit = 8;
    }
    if (kw.length === 0) {
      return Websites.find({}, {sort: {upVote: -1, createdOn: 1, limit: limit}});
    }

    // copy search code from https://www.okgrow.com/posts/guide-to-full-text-search-in-meteor
    // the code was for publication but we are using it here in a method
    var result = Websites.find({ $text: {$search: kw} }, {
      // `fields` is where we can add MongoDB projections. Here we're causing
      // each document published to include a property named `score`, which
      // contains the document's search rank, a numerical value, with more
      // relevant documents having a higher score.
      fields: {
        score: {$meta: "textScore"}
      },
      // This indicates that we wish the publication to be sorted by the
      // `score` property specified in the projection fields above.
      sort: {
        score: {$meta: "textScore"},
        upVote: -1,
        createdOn: 1
      },
      limit: limit
    });
    return result.fetch();
  }
});
