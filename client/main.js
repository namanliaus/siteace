/// routing

Router.configure({
  layoutTemplate: 'ApplicationLayout'
});

Router.route('/', function () {
  this.render('navbar', {
    to:"navbar"
  });
  this.render('main', {
    to:"main"
  });
});

Router.route('/site/:_id', function () {
  this.render('navbar', {
    to:"navbar"
  });
  this.render('website_details', {
    to:"main",
    data: function() {
      return Websites.findOne(this.params._id);
    }
  });
});

Accounts.ui.config({
  passwordSignupFields: "USERNAME_AND_EMAIL"
});


/// infiniscroll
Session.set("urlLimit", 8);
lastScrollTop = 0;
$(window).scroll(function(event) {
  // test if we are near the bottom of the window
  if($(window).scrollTop() + $(window).height() > $(document).height() - 100) {
    // where are we in the page?
    var scrollTop = $(this).scrollTop();
    // test if we are going down
    if (scrollTop > lastScrollTop){
      // yes we are heading down...
      Session.set("urlLimit", Session.get("urlLimit") + 4);
    }

    lastScrollTop = scrollTop;
  }
});


/////
// template helpers
/////

Template.website_list.onCreated(function() {
  var instance = this;
  // make it reactive way by saving websites to reactive var instead of showing them directly
  instance.websites = new ReactiveVar();
});

Template.website_list.helpers({
  // Reactively search websites using keywords. Everytime search text changes, this helper runs.
  search: function() {
    var instance = Template.instance();
    var searchText = Session.get("searchText");
    if (!searchText) {
      // no search text, return everything
      var sites = Websites.find({}, {sort: {upVote: -1, createdOn: 1}, limit: Session.get("urlLimit")});
      instance.websites.set(sites);
      return;
    }
    Meteor.call("search", searchText, Session.get("urlLimit"), function(err, result) {
      if (err) {
        console.error(err);
        return false;
      }
      instance.websites.set(result);
    });
  },
  websites: function() {
    // Return the websites stored within the reactive variable.
    // They are fetched in search helper
    var sites = Template.instance().websites.get();
    if (sites) {
      return sites;
    }
    return [];
  }
});

Template.website_item.helpers({
  getUpVote: function() {
    return this.upVote || 0;
  },
  getDownVote: function() {
    return this.downVote || 0;
  },
  getCommentCount: function() {
    return (this.comments && this.comments.length) || 0;
  },
  isOwner: function() {
    if (this.createdBy === Meteor.userId()) {
      return true;
    }
    return false;
  },
});

Template.website_item.events({
  "click .js-upvote":function(event){
    // example of how you can access the id for the website in the database
    // (this is the data context for the template)
    var website_id = this._id;
    console.log("Up voting website with id "+website_id);

    Websites.update({_id: website_id}, {
      $inc: {upVote: 1},
      $push: {votes: Meteor.userId()} // save userId for site recommendations based on votes
    });

    return false;// prevent the button from reloading the page
  },
  "click .js-downvote":function(event){

    // example of how you can access the id for the website in the database
    // (this is the data context for the template)
    var website_id = this._id;
    console.log("Down voting website with id "+website_id);

    Websites.update({_id: website_id}, {$inc: {downVote: 1}});

    return false;// prevent the button from reloading the page
  },
  "click #remove": function(event, instance) {
    Websites.remove(this._id);
    return false;
  },
})

Template.website_form.helpers({
  errorMessage: function() {
    return Session.get("errorMessage");
  }
});

Template.website_form.events({
  "click .js-toggle-website-form":function(event){
    $("#website_form").toggle('slow');
  },
  "change .js-text-input, keydown .js-text-input": function(event, instance) {
    // clear the error message
    Session.set("errorMessage", undefined);
  },
  "change #url": function(event, instance) {
    var url = event.currentTarget.value.trim();
    if (url.length === 0) {
      return false;
    }

    // client from http//localhost:3000 cannot access external websites so move
    // the function to server side
    Meteor.call("requestURL", url, function(err, result) {
      if (err) {
        console.error(err);
        return false;
      }
      if (result.length > 0) {
        // get title and description from the content with help from jQuery
        var jqContent = $("<result/>").append($.parseHTML(result));
        var titles = jqContent.find("title");
        if (titles.length > 0) {
          var title = titles.text().trim();
          // put the suggestion to the title input
          instance.$("#title").val(title);
        }
        var descriptions = jqContent.find("meta[name='description']");
        if (descriptions.length > 0) {
          var description = descriptions.attr("content");
          instance.$("#description").val(description);
        }
      }
    });

    return false;
  },
  "submit .js-save-website-form":function(event, instance){

    // here is an example of how to get the url out of the form:
    var url = event.target.url.value.trim();
    console.log("The url they entered is: "+url);

    // make sure the form has valid values for submission
    var title = event.target.title.value.trim();
    var description = event.target.description.value.trim();
    if (url.length === 0 || description.length === 0) {
      Session.set("errorMessage", "URL and description are required.");
      return false;
    }
    Websites.insert({
      title: title,
      url: url,
      description: description,
      createdOn: new Date(),
      createdBy: Meteor.userId()
    });

    // clear the form and hide it
    instance.$(".js-save-website-form")[0].reset();
    instance.$("#website_form").toggle('slow');

    return false;// stop the form submit from reloading the page
  }
});

Template.website_details.helpers({
  userName: function() {
    var user = Meteor.users.findOne(this.createdBy, {fields: {username: 1}});
    return user && user.username;
  },
  getUpVote: function() {
    return this.upVote || 0;
  },
  getDownVote: function() {
    return this.downVote || 0;
  }
});

Template.website_details.events({
  "click .js-toggle-comment-form": function(event, instance) {
    instance.$("#comment_form").toggle('slow');
    return false;
  },
  "click .js-go-back": function(event, instance) {
    history.back(1);
    return false;
  },
  "submit .js-add-comment": function(event, instance) {
    // validate the form
    var comment = event.target.comment.value.trim();
    if (comment.length === 0) {
      return false;
    }

    // insert comment to db
    Websites.update({_id: this._id}, {$push: {comments: {
      createdBy: Meteor.userId(),
      createdOn: new Date(),
      comment: comment
    }}});

    // clear the form and hide it
    instance.$(".js-add-comment")[0].reset();
    instance.$("#comment_form").toggle('slow');

    return false;
  },
});

Template.navbar.onCreated(function() {
  var instance = this;
  instance.saveSearchText = function() {
    var searchText = this.$("#search").val().trim();
    Session.set("searchText", searchText);
    Session.set("urlLimit", 8);
  }
});

Template.navbar.helpers({
  sessionSearchText: function() {
    return Session.get("searchText");
  }
});

Template.navbar.events({
  // use _.throttle with 200ms delay option to process search text less frequently
  "keyup #search": _.throttle(function(event, instance) {
    instance.saveSearchText();
    return false;
  }, 200, {leading: false}),
  "submit .navbar-form": function(event, instance) {
    instance.saveSearchText();
    return false;
  },
});

Template.website_recommendation.onCreated(function() {
  var instance = this;
  instance.websites = new ReactiveVar();
});

Template.website_recommendation.helpers({
  search: function() {
    var userId = Meteor.userId();
    var text = "";

    while (true) {
      // copy text from title and description of the sites user has upvoted
      var upvoteSites = Websites.find({votes: userId});
      upvoteSites.forEach(function(site) {
        text += " " + site.title + " " + site.description
      });

      if (upvoteSites.count() === 4) {
        // break the loop since limit has reached
        break;
      }

      // copy text from comments of user
      var commentSites = Websites.find({"comments.createdBy": userId});
      commentSites.forEach(function(site) {
        site.comments.forEach(function(comment) {
          if (comment.createdBy === userId) {
            text += " " + comment.comment;
          }
        });
      });
      break;
    }

    // use the text as the search for recommendations
    // remove text search operators may appear in the text
    text = text.replace(/"/g, "").replace(/-/g, "");
    console.log("Text for recommendations:", text);
    var instance = Template.instance();
    Meteor.call("search", text, 4, function(err, result) {
      if (err) {
        console.error(err);
        return false;
      }
      instance.websites.set(result);
    });
  },
  websites: function() {
    return Template.instance().websites.get();
  }
});
