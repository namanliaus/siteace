Websites = new Mongo.Collection("websites");

///////
// a website may have full data:
// {
//   title: String,
//   url: String,
//   description: String,
//   upVote: Number,
//   votes: [String] // array of up voted userIds
//   downVote: Number,
//   createdOn: Date,
//   createdBy: String,
//   comments: [{ // array of comments with owners and dates.
//     createdOn: Date,
//     createdBy: String,
//     comment: String
//   }]
// }

Websites.allow({
  update: function(userId, doc) {
    // logged in users only
    if (Meteor.user()) {
      return true;
    }

    return false;
  },
  insert: function() {
    // logged in users only
    if (Meteor.user()) {
      return true;
    }

    return false;
  },
  remove: function(userId, doc) {
    // logged in users only
    if (!Meteor.user()) {
      return false;
    }
    // user can remove oneself created links
    if (doc.createdBy === userId) {
      return true;
    }
    return false;
  }
})
